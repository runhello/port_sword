# port_sword

Quick mac port of Sword In Hand by Jeff Lait. Download playable version at http://vote.grumpybumpers.com/j/sword/1/sword_in_hand.zip

See mac/compile.txt for build instructions.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/port_sword).**
